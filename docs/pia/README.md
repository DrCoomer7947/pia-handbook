---
sidebar: auto
---

# PIA
## Group Overview

The Pinewood Intelligence Agency, owned and managed by Diddleshot, performs special duties at Pinewood Builders games. 

The group is mainly focused on providing moderation across all PB games. The group is invite-only.

☛Do not ask to join.

## P.I.A Staff
### Head of Intelligence:	
- Diddleshot  

### Alpha Level Agent:	
- LENEMAR, 
- Csdi, 
- Irreflexive  

### Agent:	
- WickyTheUnicorn, 
- TheGreatOmni, 
- TenX29, 
- Erika1942, 
- RogueVader1996, 
- Vah_Medoh,
- Supremo_miguel
- Coasterteam

### On Hiatus:	
- None
